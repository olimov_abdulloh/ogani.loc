<?php
/**
 * Created by PhpStorm.
 * User: Abdulloh Olimov
 * Date: 14.11.2020
 * Time: 11:38
 */


namespace frontend\controllers;


use backend\models\Cart;
use backend\models\Favorites;
use backend\models\Products;
use yii\web\Controller;
use Yii;
class ProductsController extends Controller
{	
    public function actionView($slug){
        $model = Products::find()
                    ->with(['category'])
                    ->where(['keyword' => $slug])->one();
        if($model == null) $this->redirect('/site/error');
        return $this->render('view',[
            'model' => $model,
        ]);
    }

    public function actionSetFavorites($id)
    {
        $user = Yii::$app->user->identity;
        if($user){
            $favorites = Favorites::find()->where(['user_id' => $user->id,'product_id' => $id])->one();
            if($favorites){
                $favorites->delete();
            }else {
                $favorites = new Favorites();
                $favorites->user_id = $user->id;
                $favorites->product_id = $id;
                $favorites->save(false);
            }
            return true;
        } else {
            if(isset($_SESSION['product'][$id])){
                unset($_SESSION['product'][$id]);
            }
            else {
                $_SESSION['product'][$id]=$id;
            }

            return $id;
        }
    }

    public function actionSetCard($id)
    {
        $user = Yii::$app->user->identity;
        if($user){
            $favorites = Cart::find()->where(['user_ids' => $user->id,'product_id' => $id])->one();
            if($favorites){
                $favorites->delete();
            }else {
                $favorites = new Cart();
                $favorites->users_id = $user->id;
                $favorites->product_id = $id;
                $favorites->save(false);
            }
            return true;
        } else {
            if(isset($_SESSION['cart'][$id])){
                unset($_SESSION['cart'][$id]);
            }
            else {
                $_SESSION['cart'][$id]=$id;
            }
            return $id;
        }
    }

    public function actionTest(){
        echo '<pre>';
        print_r($_SESSION);
        die;
    }

}