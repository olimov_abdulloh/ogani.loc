<?php
/* 
    Веб разработчик: Abdulloh Olimov 
*/

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class Register extends Model
{
    public $login;
    public $email;
    public $phone;

    public $age;
    public $gender;
    public $price;

    const SCENARIOS_SIGNUP = 'signup';
    const SCENARIOS_AGE = 'age';
    const SCENARIOS_PRICE = 'price';


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email' , 'phone' , 'login' , 'age','gender', 'price'] ,'safe'],
            ['login', 'trim'],
            ['login', 'required'],
            ['login', 'string'],
            ['login' , 'validationPhoneNumber'],
            ['age' ,'validateAge'],
            ['price' ,'validatePrice'],

        ];
    }

    public  function scenarios()
    {
        return [
            self::SCENARIOS_SIGNUP => ['login'],
            self::SCENARIOS_AGE =>['age'],
            self::SCENARIOS_PRICE =>['gender', 'price'],

        ];
    }

    public function  getGender()
    {
        return [
            1 => 'Erkak',
            2 => 'Ayol',
        ];
    }

    public function validateAge($attribute,$params)
    {
        if($this->age < 18)   $this->addError($attribute, 'Yoshingiz 18 dan kichik');
    }
    public function validatePrice($attribute,$params)
    {
        if($this->gender < 2 && $this->price < 10000)  $this->addError($attribute, 'ссылка на видео неверна');
    }

    public function validationPhoneNumber($attribute,$params)
    {
        $subject = $this->login;
        $pattern = '#^(?:https?://)?';    # Optional URL scheme. Either http or https.
        $pattern .= '(?:www\.)?';         #  Optional www subdomain.
        $pattern .= '(?:';                #  Group host alternatives:
        $pattern .=   'youtu\.be/';       #    Either youtu.be,
        $pattern .=   '|youtube\.com';    #    or youtube.com
        $pattern .=   '(?:';              #    Group path alternatives:
        $pattern .=     '/embed/';        #      Either /embed/,
        $pattern .=     '|/v/';           #      or /v/,
        $pattern .=     '|/watch\?v=';    #      or /watch?v=,
        $pattern .=     '|/watch\?.+&v='; #      or /watch?other_param&v=
        $pattern .=   ')';                #    End path alternatives.
        $pattern .= ')';                  #  End host alternatives.
        $pattern .= '([\w-]{11})';        # 11 characters (Length of Youtube video ids).
        $pattern .= '(?:.+)?$#x';         # Optional other ending URL parameters.

        preg_match($pattern,$subject, $matches_a_z, PREG_OFFSET_CAPTURE);


        if($matches_a_z){
            echo '<pre>';
            print_r($matches_a_z); die;
        }
        else {
            echo
            'yoo';
        }


    }


}
