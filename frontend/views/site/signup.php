<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'signup';
?>


<div class="contact-form spad ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="contact__form__title">
                    <h2>Sing up</h2>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <div class="row">
            <div class="col-lg-4 col-md-4"></div>

            <div class="col-lg-4 col-md-4">
                <p>Username</p>
                <?= $form->field($model, 'login')->textInput(['autofocus' => true])->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4"></div>
            <div class="col-lg-4 col-md-4">
                <p>Email</p>
                <?= $form->field($model, 'email')->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4"></div>
            <div class="col-lg-4 col-md-4">
                <p>Password</p>
                <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4"></div>
            <div class="col-lg-4">
                <button type="submit" class="site-btn">Signup</button>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-4 col-md-4"></div>
            <div class="col-lg-4 col-md-4">
                <a href="/site/login"><h5>Login</h5></a>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

