<!-- Featured Section Begin -->
<?php
/* @var $dataProvider*/

use backend\models\Products;
use yii\widgets\LinkPager;
use \yii\widgets\Pjax;
?>
<section class="featured spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Featured Product</h2>
                </div>
            </div>
        </div>
        <div class="row featured__filter" id="products">
            <?php foreach ($dataProvider->getModels() as $value):?>
                <?php
//
                $class = '';
                if($value->favorites){
                    $class = 'favorites';
                }elseif(isset($_SESSION['product'][$value->id]) && $_SESSION['product'][$value->id] == $value->id){
                    $class = 'favorites';
                }

                $class_2 = '';
                if($value->cart){
                    $class_2 = 'favorites';
                }elseif(isset($_SESSION['cart'][$value->id]) && $_SESSION['cart'][$value->id] == $value->id){
                    $class_2 = 'favorites';
                }
                ?>
                <div class="col-lg-3 col-md-4 col-sm-6 mix" >
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg " data-setbg="<?= Products::getImageAdress($value->getImagesOne())?>" style="cursor: pointer;">
                            <ul class="featured__item__pic__hover">
                                <li><a id="favorites<?=$value->id?>" class="<?= $class?>" onclick="setFavorites(<?=$value->id?>)"><i class="fa fa-heart"></i></a></li>
                                <li><a href="#"><i class="fa fa-retweet"></i></a></li>
                                <li><a id="card<?=$value->id?>" class="<?= $class_2?>" onclick=" setCard(<?=$value->id?>)"><i class="fa fa-shopping-cart "></i></a></li>
                            </ul>
                        </div>
                        <div class="featured__item__text">
                            <h6><a href="<?= \yii\helpers\Url::to(['products/view' ,'slug' => $value->keyword])?>"><?= $value->title?></a></h6>
                            <h5><?= $value->price.' '.$value->getCurrencyIcon()?></h5>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <? echo LinkPager::widget([
        'pagination' => $dataProvider->pagination,

    ]);
    ?>
</section>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div class="banner__pic hero__item set-bg" style="height:250px;" data-setbg="<?= \backend\models\Banner::getImageSite($banners[2]['image'])?>">
                <div class="hero__text">
                    <span>FRUIT FRESH</span>
                    <h2><?= $banners[2]['title']?></h2>
                    <p><?= $banners[2]['text']?></p>
                    <a href="<?= $banners[2]['url_banner']?>" class="primary-btn">SHOP NOW</a>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class=" banner__pic hero__item set-bg" style="height:250px;" data-setbg="<?= \backend\models\Banner::getImageSite($banners[3]['image'])?>">
                <div class="hero__text">
                    <span>FRUIT FRESH</span>
                    <h2><?= $banners[3]['title']?></h2>
                    <p><?= $banners[3]['text']?></p>
                    <a href="<?= $banners[3]['url_banner']?>" class="primary-btn">SHOP NOW</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8" async defer>
function setFavorites(id){
	document.getElementById("favorites"+id).classList.toggle('favorites');
	$.ajax({
       type: 'GET',
       data: {id: id},
       url: '/products/set-favorites',
       success: function(data){
            $.pjax.reload({container:"#products"});
       }
    });
}

function setCard(id){
	document.getElementById("card"+id).classList.toggle('favorites');
	$.ajax({
       type: 'GET',
       data: {id: id},
       url: '/products/set-card',
       success: function(data){
       }
    });
}

</script>
<!-- Featured Section End -->
