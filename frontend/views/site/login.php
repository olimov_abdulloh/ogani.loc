<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\bootstrap\ActiveForm;
$this->title = 'sign in';

?>


<div class="contact-form spad ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="contact__form__title">
                    <h2>Sign in</h2>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <div class="row">
                <div class="col-lg-4 col-md-4"></div>

                <div class="col-lg-4 col-md-4">
                    <p>Login</p>
                    <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4"></div>
                <div class="col-lg-4 col-md-4">
                    <p>Password</p>
                    <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4"></div>
                <div class="col-lg-4">
                    <button type="submit" class="site-btn">Sign in</button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-4 col-md-4"></div>
                <div class="col-lg-4 col-md-4">
                    <a href="/site/signup"><h5>SignUp</h5></a>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
