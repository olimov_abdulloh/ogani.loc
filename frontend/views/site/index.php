<?php
/* @var $category  */
?>
<?= $this->render('index-body',[
    'category' => $category,
    'topProducts'=>$topProducts,
    'text' => $text,
    'banners' => $banners,
]);?>
<?= $this->render('index-items',[
    'dataProvider' => $dataProvider,
    'banners' => $banners,
]);?>

