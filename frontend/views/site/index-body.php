<?php

/* @var $category  */

$this->title = 'Ogani';
/* @var $topProducts*/
/* @var $text */

use yii\bootstrap\ActiveForm;

?>
<!-- Hero Section Begin -->
<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>All Categories</span>
                    </div>
                    <ul>
                        <?php foreach ($category as $value): ?>
                            <li><a href="<?= \yii\helpers\Url::to(['/site/index' ,'category_id' => $value->id])?>"><?= $value->name?></a></li>
                        <?php endforeach;?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="hero__search">
                    <div class="hero__search__form">
                        <?php ActiveForm::begin(['action' => 'index','method'=>'get']);?>
                            <input type="text" placeholder="What do yo u need?" name="text" value="<?= $text?>">
                            <button type="sumbit" class="site-btn">SEARCH</button>
                       <?php ActiveForm::end();?>
                    </div>
                    <div class="hero__search__phone">
                        <div class="hero__search__phone__icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="hero__search__phone__text">
                            <h5>+65 11.188.888</h5>
                            <span>support 24/7 time</span>
                        </div>
                    </div>
                </div>
                <div class="hero__item set-bg" data-setbg="<?= \backend\models\Banner::getImageSite($banners[1]['image'])?>">
                    <div class="hero__text">
                        <span>FRUIT FRESH</span>
                        <h2><?= $banners[1]['title']?></h2>
                        <p><?= $banners[1]['text']?></p>
                        <a href="<?= $banners[1]['url_banner']?>" class="primary-btn">SHOP NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->
<!-- Categories Section Begin -->
<section class="categories">
    <div class="container">
        <div class="row">
            <div class="categories__slider owl-carousel">
                <?php foreach ($topProducts as $value):?>
                    <div class="col-lg-3"  onclick="window.location.href=''" style="cursor:pointer;">
                        <div class="categories__item set-bg" data-setbg="<?=\backend\models\Products::getImageAdress($value->getImagesOne())?>">
                            <h5><a href="<?= \yii\helpers\Url::to(['products/view' ,'slug' => $value->keyword])?>"><?= $value->title?></a></h5>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</section>
<!-- Categories Section End -->
