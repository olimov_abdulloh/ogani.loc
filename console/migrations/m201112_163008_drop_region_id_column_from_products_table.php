<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%products}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%regions}}`
 */
class m201112_163008_drop_region_id_column_from_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `{{%regions}}`
        $this->dropForeignKey(
            '{{%fk-products-region_id}}',
            '{{%products}}'
        );

        // drops index for column `region_id`
        $this->dropIndex(
            '{{%idx-products-region_id}}',
            '{{%products}}'
        );

        $this->dropColumn('{{%products}}', 'region_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%products}}', 'region_id', $this->integer());

        // creates index for column `region_id`
        $this->createIndex(
            '{{%idx-products-region_id}}',
            '{{%products}}',
            'region_id'
        );

        // add foreign key for table `{{%regions}}`
        $this->addForeignKey(
            '{{%fk-products-region_id}}',
            '{{%products}}',
            'region_id',
            '{{%regions}}',
            'id',
            'CASCADE'
        );
    }
}
