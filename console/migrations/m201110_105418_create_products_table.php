<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%sub_categories}}`
 * - `{{%currency}}`
 * - `{{%regions}}`
 * - `{{%districts}}`
 */
class m201110_105418_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'subcategory_id' => $this->integer(),
            'text' => $this->text(),
            'price' => $this->float(),
            'old_price' => $this->float(),
            'currency_id' => $this->integer(),
            'region_id' => $this->integer(),
            'district_id' => $this->integer(),
            'status' => $this->integer(),
            'top' => $this->boolean(),
        ]);

        // creates index for column `subcategory_id`
        $this->createIndex(
            '{{%idx-products-subcategory_id}}',
            '{{%products}}',
            'subcategory_id'
        );

        // add foreign key for table `{{%sub_categories}}`
        $this->addForeignKey(
            '{{%fk-products-subcategory_id}}',
            '{{%products}}',
            'subcategory_id',
            '{{%sub_categories}}',
            'id',
            'CASCADE'
        );

        // creates index for column `currency_id`
        $this->createIndex(
            '{{%idx-products-currency_id}}',
            '{{%products}}',
            'currency_id'
        );

        // add foreign key for table `{{%currency}}`
        $this->addForeignKey(
            '{{%fk-products-currency_id}}',
            '{{%products}}',
            'currency_id',
            '{{%currency}}',
            'id',
            'CASCADE'
        );

        // creates index for column `region_id`
        $this->createIndex(
            '{{%idx-products-region_id}}',
            '{{%products}}',
            'region_id'
        );

        // add foreign key for table `{{%regions}}`
        $this->addForeignKey(
            '{{%fk-products-region_id}}',
            '{{%products}}',
            'region_id',
            '{{%regions}}',
            'id',
            'CASCADE'
        );

        // creates index for column `district_id`
        $this->createIndex(
            '{{%idx-products-district_id}}',
            '{{%products}}',
            'district_id'
        );

        // add foreign key for table `{{%districts}}`
        $this->addForeignKey(
            '{{%fk-products-district_id}}',
            '{{%products}}',
            'district_id',
            '{{%districts}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%sub_categories}}`
        $this->dropForeignKey(
            '{{%fk-products-subcategory_id}}',
            '{{%products}}'
        );

        // drops index for column `subcategory_id`
        $this->dropIndex(
            '{{%idx-products-subcategory_id}}',
            '{{%products}}'
        );

        // drops foreign key for table `{{%currency}}`
        $this->dropForeignKey(
            '{{%fk-products-currency_id}}',
            '{{%products}}'
        );

        // drops index for column `currency_id`
        $this->dropIndex(
            '{{%idx-products-currency_id}}',
            '{{%products}}'
        );

        // drops foreign key for table `{{%regions}}`
        $this->dropForeignKey(
            '{{%fk-products-region_id}}',
            '{{%products}}'
        );

        // drops index for column `region_id`
        $this->dropIndex(
            '{{%idx-products-region_id}}',
            '{{%products}}'
        );

        // drops foreign key for table `{{%districts}}`
        $this->dropForeignKey(
            '{{%fk-products-district_id}}',
            '{{%products}}'
        );

        // drops index for column `district_id`
        $this->dropIndex(
            '{{%idx-products-district_id}}',
            '{{%products}}'
        );

        $this->dropTable('{{%products}}');
    }
}
