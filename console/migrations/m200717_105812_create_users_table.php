<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%companies}}`
 */
class m200717_105812_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255)->comment("ФИО"),
            'login' => $this->string(255)->comment("Логин"),
            'avatar' => $this->string(255)->comment("Фото"),
            'password' => $this->string(255)->comment("Пароль"),
            'phone' => $this->string(255)->comment("Телефон"),
            'status' => $this->integer()->comment("Статус"),
            'email' => $this->string(255)->comment("Эмаил"),
            'balans' => $this->float()->comment("Баланс"),
            'date_cr' =>$this->datetime()->comment("Дата создания"),
            'date_cr' =>$this->datetime()->comment("Последное активность"),
            'access_token' => $this->string(255)->comment("Токен"),
            'in_phone' => $this->boolean()->comment("по телефону"),
            'in_email' => $this->boolean()->comment("по эмаил"),
            'expiret_at' => $this->integer()->comment("Жизненной цикл"),
            'sms_code' => $this->integer()->comment("Смс код"),
            'status' => $this->integer()->comment("Статус"),
        ]);

          $this->insert('users',array(
            'id'=>1,
            'fio'=>'А.Ю Олимов',
            'status'=>1,
            'date_cr'=>date('Y-m-d H:i:s'),
            'login'=>'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
          ));
    }

    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
