<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%products}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%districts}}`
 */
class m201112_163107_drop_district_id_column_from_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drops foreign key for table `{{%districts}}`
        $this->dropForeignKey(
            '{{%fk-products-district_id}}',
            '{{%products}}'
        );

        // drops index for column `district_id`
        $this->dropIndex(
            '{{%idx-products-district_id}}',
            '{{%products}}'
        );

        $this->dropColumn('{{%products}}', 'district_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%products}}', 'district_id', $this->integer());

        // creates index for column `district_id`
        $this->createIndex(
            '{{%idx-products-district_id}}',
            '{{%products}}',
            'district_id'
        );

        // add foreign key for table `{{%districts}}`
        $this->addForeignKey(
            '{{%fk-products-district_id}}',
            '{{%products}}',
            'district_id',
            '{{%districts}}',
            'id',
            'CASCADE'
        );
    }
}
