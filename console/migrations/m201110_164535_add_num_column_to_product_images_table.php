<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%product_images}}`.
 */
class m201110_164535_add_num_column_to_product_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%product_images}}', 'num', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%product_images}}', 'num');
    }
}
