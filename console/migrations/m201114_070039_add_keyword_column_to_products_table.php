<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%products}}`.
 */
class m201114_070039_add_keyword_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%products}}', 'keyword', $this->string(255));
        $this->addColumn('{{%products}}', 'date_cr', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%products}}', 'keyword');
        $this->dropColumn('{{%products}}', 'date_cr');
    }
}
