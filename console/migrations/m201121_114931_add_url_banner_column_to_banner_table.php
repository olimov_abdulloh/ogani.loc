<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%banner}}`.
 */
class m201121_114931_add_url_banner_column_to_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%banner}}', 'url_banner', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%banner}}', 'url_banner');
    }
}
