<?php

namespace common\models;

use backend\models\Cart;
use backend\models\Favorites;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;
    const  STATUS_ACTIVE = 1;
    const USER_ROLE = 3;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Логин или пароль введен не верно.');
            }
            if($user->status != 1){
                $this->addError($attribute, 'Confirm that the email is yours! To do this, click on the link sent to your email');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $user = $this->getUser();
        if ($this->validate() && $user->status == 1) {
            return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    public function setFavorites()
    {
        $user = Yii::$app->user->identity;
        if($user){
        if(isset($_SESSION['product'])){

            foreach ($_SESSION['product'] as $value){

                    $favorites = Favorites::find()->where(['user_id' => $user->id,'product_id' => $value])->one();
                    if($favorites){
                        continue;
                    }else {
                        $favorites = new Favorites();
                        $favorites->user_id = $user->id;
                        $favorites->product_id = $value;
                        $favorites->save(false);
                    }
                }
            }
        }
        unset($_SESSION['product']);
        $this->setCart();
        return true;
    }

    public function setCart()
    {
        $user = Yii::$app->user->identity;
        if($user){
            if(isset($_SESSION['cart'])){

                foreach ($_SESSION['cart'] as $value){

                    $favorites = Cart::find()->where(['users_id' => $user->id,'product_id' => $value])->one();
                    if($favorites){
                        continue;
                    }else {
                        $favorites = new Cart();
                        $favorites->users_id = $user->id;
                        $favorites->product_id = $value;
                        $favorites->save(false);
                    }
                }
            }
        }
        unset($_SESSION['cart']);
        return true;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
