<?php

namespace backend\modules\markaz\controllers;

use yii\web\Controller;

/**
 * Default controller for the `markaz` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
