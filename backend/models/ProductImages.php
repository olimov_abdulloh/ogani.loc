<?php

namespace backend\models;

use Yii;
use yii\imagine\Image;

/**
 * This is the model class for table "product_images".
 *
 * @property int $id
 * @property string|null $filename
 * @property int|null $product_id
 * @property string|null $image_o
 * @property string|null $image_m
 * @property string|null $image_s
 *
 * @property Products $product
 */
class ProductImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'default', 'value' => null],
            [['product_id'], 'integer'],
            [['filename', 'image_o', 'image_m', 'image_s'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'product_id' => 'Product ID',
            'image_o' => 'Image O',
            'image_m' => 'Image M',
            'image_s' => 'Image S',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }


    public function deleteAllImage()
    {
        $this->deleteImage($this->image_o);
        $this->deleteImage($this->image_m);
        $this->deleteImage($this->image_s);
    }

    public function deleteImage($image)
    {
        Yii::getAlias('@webroot/uploads/product/'.$image);
    }

}
