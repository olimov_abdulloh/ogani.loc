<?php

namespace backend\models;
use Yii;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $subcategory_id
 * @property string|null $text
 * @property float|null $price
 * @property float|null $old_price
 * @property int|null $currency_id
 * @property int|null $status
 * @property bool|null $top
 * *@property int|null $category_id
 *
 * @property Currency $currency
 * @property SubCategories $subcategory
 * @property Categories $category

 */
class Products extends \yii\db\ActiveRecord
{
    const ITEM_ACTIVE = 1;
    const ITEM_INACTIVE = 2;
    const ITEM_DRAFT = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'skeeks\yii2\slug\SlugBehavior',
                'slugAttribute' => 'keyword',                      //The attribute to be generated
                'attribute' => 'title',                          //The attribute from which will be generated
                // optional params
                'maxLength' => 100,                              //Maximum length of attribute slug
                'minLength' => 3,                               //Min length of attribute slug
                'ensureUnique' => true,
                'slugifyOptions' => [
                    'lowercase' => true,
                    'separator' => '-',
                    'trim' => true,
                    //'regexp' => '/([^A-Za-z0-9]|-)+/',
                    'rulesets' => ['russian'],
                    //@see all options https://github.com/cocur/slugify
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subcategory_id', 'currency_id', 'status'], 'default', 'value' => null],
            [['subcategory_id', 'currency_id', 'status','category_id'], 'integer'],
            [['text'], 'string'],
            [['date_cr'], 'safe'],
            [['price', 'old_price'], 'number'],
            [['top'], 'boolean'],
            [['title','keyword'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['subcategory_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubCategories::className(), 'targetAttribute' => ['subcategory_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'subcategory_id' => 'Суб катекорий',
            'text' => 'Описания',
            'price' => 'Цена',
            'old_price' => 'Старая цена',
            'currency_id' => 'Валюты',
            'status' => 'Статус',
            'top' => 'Премиум',
            'category_id' => 'Категория',
        ];
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }
    public function getFavorites()
    {
        $user_id = Yii::$app->user->identity != null ? Yii::$app->user->identity->id : null;
        return $this->hasOne(Favorites::className(), ['product_id' => 'id'])->where(['user_id' => $user_id]);
    }

    public function getCart()
    {
        $user_id = Yii::$app->user->identity != null ? Yii::$app->user->identity->id : null;
        return $this->hasOne(Cart::className(), ['product_id' => 'id'])->where(['users_id' => $user_id]);
    }



    /**
     * Gets query for [[Subcategory]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubcategory()
    {
        return $this->hasOne(SubCategories::className(), ['id' => 'subcategory_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public static function getStatus()
    {
        return ArrayHelper::map([
            ['id' => self::ITEM_ACTIVE, 'name' => 'Активный'],
            ['id' => self::ITEM_INACTIVE, 'name' => 'Не активен'],
            ['id' => self::ITEM_DRAFT, 'name' => 'Чернавик'],
        ], 'id', 'name');
    }

    public function getStatusDescription()
    {
        switch ($this->status) {
            case 1: return "Активный";
            case 2: return "Не активен";
            case 3: return "Чернавик";
            default: return "Неизвестно";
        }
    }

    public function getCurrencyList()
    {
        $currency = Currency::find()->all();
        return ArrayHelper::map($currency, 'id', 'name');
    }

    public function getCategoryList()
    {
        $category = Categories::find()->all();
        return ArrayHelper::map($category, 'id', 'name');
    }

    public function getSubategoryid($id)
    {
        return ArrayHelper::map(SubCategories::find()->where(['category_id' => $id])->all(), 'id', 'name');
    }

    public static function getImageAdress($img)
    {
        $dir = '/product';
        $path = 'http://'.$_SERVER['SERVER_NAME'].'/admin/uploads/'.$dir.'/'.$img;
        return $path;
    }

    public  function setImageChange($post)
    {
        $dir = 'uploads/trash/';
        $i = ProductImages::find()->where(['product_id' => $this->id])->count()+1;
        $uploaded_files = null;
        $uploaded_files = isset($post['uploaded_files']) ? explode(',',$post['uploaded_files']) : null;
        if (isset($uploaded_files[0]) && $uploaded_files[0] != null){
            foreach ($uploaded_files as $value){
                $time = time();
                $image =  Yii::getAlias('@webroot/uploads/trash/'.$value);

                list($sizeWidth, $sizeHeight) = getimagesize($image);
                $filename_o = str_replace(' ','-',$this->title).'-'.$this->id.'-o-'.$i.$time.'.jpg';
                $filename_s = str_replace(' ','-',$this->title).'-'.$this->id.'-s-'.$i.$time.'.jpg';
                $filename_m = str_replace(' ','-',$this->title).'-'.$this->id.'-m-'.$i.$time.'.jpg';
                Image::thumbnail($image,$sizeWidth,$sizeHeight)
                    ->save(Yii::getAlias('@webroot/uploads/product/'.$filename_o), ['quality' => 65]);

                Image::thumbnail($image,$sizeWidth,$sizeHeight)
                    ->save(Yii::getAlias('@webroot/uploads/product/'.$filename_s), ['quality' => 45]);
                Image::thumbnail($image,$sizeWidth,$sizeHeight)
                    ->save(Yii::getAlias('@webroot/uploads/product/'.$filename_m), ['quality' => 35]);
                $model = new ProductImages();
                $model->product_id = $this->id;
                $model->image_m = $filename_m;
                $model->image_s = $filename_s;
                $model->image_o = $filename_o;
                $model->num = $i;
                $model->save();
                unlink($image);
                $i++;
            }
        }
    }

    //barcha rasmlar ruyhati
    public function getImages()
    {
        $imgs = ProductImages::find()->where(['product_id' => $this->id])->orderBy(['id' => SORT_ASC])->all();
        return ArrayHelper::map($imgs,'id','image_s');
    }

    public function getImagesOne()
    {
        $imgs = ProductImages::find()->where(['product_id' => $this->id])->orderBy(['id' => SORT_ASC])->one();
        return $imgs  != null ? $imgs->image_s : null;
    }

    public function getCurrencyIcon(){
        if($this->currency_id == 1) {
            return 'sum';
        }elseif ($this->currency_id == 2){
            return '<i class="fa fa-rub"></i>';
        }
        else {
            return '<i class="fa fa-dollar"></i>';
        }
    }


}
