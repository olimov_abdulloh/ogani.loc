<?php

namespace backend\models;


use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property int $role_id
 */
class Users extends ActiveRecord
{
    const USER_STATUS_ACTIVE    = 1;
    const USER_STATUS_IN_ACTIVE = 2;
    const USER_STATUS_BLOCKED   = 3; 
    const USER_STATUS_DELETED   = 4;
    const GENERAL_ADMIN = 1;

    const ROLE_ADMIN = 1;
    const ROLE_MODERATOR = 2;
    const ROLE_USER = 3;


    public $new_password;
    public $image;
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status','role_id'] ,'integer'],
            [['fio', 'login', 'password','phone','email', 'balans', 'date_cr','avatar'], 'string', 'max' => 255],
            [['fio', 'login', 'password', 'status'], 'required'],
            [['login'],  'unique'],
            [['email'],  'email'],
            [['date_cr','image' ,'new_password'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'new_password' => 'Новый пароль',
            'status' => 'Статус',
            'phone' => 'Телефон',
            'email' => 'Email',
            'balans' => 'Баланс',
            'date_cr' => 'Дата создания',
            'avatar' => 'Фото',
            'image' => 'Фото',
            'role_id' => 'Роль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {   
            $user = Yii::$app->user->identity;
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            $this->date_cr = date('Y-m-d H:i');
        }

        if($this->new_password != null) $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public static function getStatus()
    {
        return ArrayHelper::map([
            ['id' => self::USER_STATUS_ACTIVE, 'name' => 'Активный'],
            ['id' => self::USER_STATUS_IN_ACTIVE, 'name' => 'Не активен'],
            ['id' => self::USER_STATUS_BLOCKED, 'name' => 'Блокированный'],
            ['id' => self::USER_STATUS_DELETED, 'name' => 'Удален'],
        ], 'id', 'name');
    }

    public function getStatusDescription()
    {
        switch ($this->status) {
            case 1: return "Активный";
            case 2: return "Не активен";
            case 3: return "Блокированный";
            case 4: return "Удален";
            default: return "Неизвестно";
        }
    }

    public static function getRole()
    {
        return ArrayHelper::map([
            ['id' => self::ROLE_ADMIN, 'name' => 'Администратор'],
            ['id' => self::ROLE_MODERATOR, 'name' => 'Модератор'],
            ['id' => self::ROLE_USER, 'name' => 'Ползователь'],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {
        switch ($this->role_id) {
            case 1: return "Администратор";
            case 2: return "Модераторн";
            case 3: return "Ползователь";
            default: return "Неизвестно";
        }
    }

    public function uploadAvatar()
    {   
        $fileName = "";
        $fileName = $this->id . '-' . Yii::$app->security->generateRandomString() . '.' . $this->image->extension;
        if(!empty($this->image))
        {   
            if(file_exists('uploads/avatars/'.$this->avatar) && $this->avatar != null)
            {
                unlink('uploads/avatars/'.$this->avatar);
            }

            $this->image->saveAs('uploads/avatars/'.$fileName);
            Yii::$app->db->createCommand()->update('users', ['avatar' => $fileName], [ 'id' => $this->id ])->execute();
        }
    }

    public  function getAvatar()
    {
        if (!file_exists('uploads/avatars/'.$this->avatar) || $this->avatar == '') {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/admin/img/nouser.png';
        } else {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/admin/uploads/avatars/'.$this->avatar;
        }
        return $path;
    }
    
}
