<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $text
 * @property string|null $key
 * @property string|null $image
 * @property bool|null $status
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['file'],'safe'],
            [['status'], 'boolean'],
            [['title', 'key','url_banner', 'image'], 'string', 'max' => 255],
            [['title','url_banner'], 'required'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'key' => 'Кей',
            'image' => 'Фото',
            'status' => 'Статус',
            'file' => 'Фото',
            'url_banner' => 'Линк',
        ];
    }

    public function uploadImage()
    {
        $fileName = "";
        $fileName = $this->id . '-' . Yii::$app->security->generateRandomString() . '.' . $this->file->extension;
        if(!empty($this->file))
        {
            if(file_exists('uploads/banner/'.$this->image) && $this->image != null)
            {
                unlink('uploads/banner/'.$this->image);
            }
            $this->file->saveAs('uploads/banner/'.$fileName);
            Yii::$app->db->createCommand()->update('banner', ['image' => $fileName], [ 'id' => $this->id ])->execute();
        }
    }

    public  function getImage()
    {
        if (!file_exists('uploads/banner/'.$this->image) || $this->image == '') {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/admin/img/no-logo.png';
        } else {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/admin/uploads/banner/'.$this->image;
        }
        return $path;
    }

    public static  function getImageSite($image)
    {
        if ($image == '' ||$image == null ) {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/admin/img/no-logo.png';
        } else {
            $path = 'http://' . $_SERVER['SERVER_NAME'].'/admin/uploads/banner/'.$image;
        }
        return $path;
    }
}
