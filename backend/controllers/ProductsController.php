<?php

namespace backend\controllers;
use backend\models\Categories;
use backend\models\Currency;
use backend\models\ProductImages;
use backend\models\SubCategories;
use Faker\Factory;
use Yii;
use backend\models\Products;
use backend\models\search\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;


/**
 * CurrencyController implements the CRUD actions for Model model.
 */
class ProductsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Model models.
     * @return mixed
     */
    public function  actionGenerate(){
        $faker = Factory::create();
        for($i = 0; $i < 100; $i++)
        {
            $model = new Products();
            $model->title = $faker->text(40);
            $model->category_id = rand(1,5);
            $model->price = rand(10 , 100);
            $model->text = $faker->text(350);
            $model->status = rand(1,3);
            $model->currency_id = rand(1,3);
            $model->top = rand(0,1);
            $model->save(false);
        }
    }

    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Products();
        if ($model->load($request->post()) && $model->save()) {
            $model->setImageChange($_POST);
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'upload_images' =>[],
                'post' => $_POST,
            ]);
        }

    }


    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $upload_images = $model->getImages();
        if ($model->load($request->post()) && $model->save()) {
            $model->setImageChange($_POST);
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'upload_images' => $upload_images,
                'post' => $_POST,
            ]);
        }
    }

    public function actionSaveImg()
    {
        $dir = '/admin/uploads/trash/';
        $path = Yii::getAlias('@app') . '/web/uploads/trash/';

        for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
                $fPath = $_POST['names'][$i];
                $ftp_path = $path.$fPath;
                if(move_uploaded_file($_FILES['file']['tmp_name'][$i], $ftp_path)){

                }
        }
        return 'http://'.$_SERVER['SERVER_NAME'] . $dir ;
    }

    public function actionDeleteImage($value,$id=null)
    {
        if($id != null){
            $images = ProductImages::find()->where(['id' => $id])->all();
            foreach ($images as $val){
                $val->deleteAllImage();
                $val->delete();
            }
        }
    }

    public function deleteImage($id){
        if($id != null){
            $images = ProductImages::find()->where(['id' => $id])->all();
            foreach ($images as $val){
                $val->deleteAllImage();
                $val->delete();
            }
        }
    }


    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($model != null){
            self::deleteImage($id);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }


    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionSubcategory($id)
    {
        $categories = Categories::find()->where(['id' => $id])->one();
        $subcategory = SubCategories::find()->where(['category_id' => $categories->id])->all();
        foreach ($subcategory as $value) {
            echo "<option value = '".$value->id."'>".$value->name."</option>" ;
        }
    }

    /**
     * Finds the Model model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Model the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
