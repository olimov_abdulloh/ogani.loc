<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'avtorizatsiya','upload'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
   public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {

            return $this->redirect(['/site/login']);
        }


        return $this->render('index',[
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['login']);
    }

    public function actionAvtorizatsiya()
    {
      if(isset(Yii::$app->user->identity->id))
      {
        return $this->render('error');
      }        
       else
        {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }

    }

    public function actionSetThemeValues()
    {
        $session = Yii::$app->session;
        if (isset($_POST['sd_position'])) $session['sd_position'] = $_POST['sd_position'];

        if (isset($_POST['header_styling'])) $session['header_styling'] = $_POST['header_styling'];

        if (isset($_POST['sd_styling'])) $session['sd_styling'] = $_POST['sd_styling'];

        if (isset($_POST['cn_gradiyent'])) $session['cn_gradiyent'] = $_POST['cn_gradiyent'];

        if (isset($_POST['cn_style'])) $session['cn_style'] = $_POST['cn_style'];

        if (isset($_POST['boxed'])) $session['boxed'] = $_POST['boxed'];

    }

    public function actionSdPosition()
    {
        $session = Yii::$app->session;
        if($session['sd_position'] != null) return $session['sd_position'];
        else return 1;
    }

    public function actionHeaderStyling()
    {
        $session = Yii::$app->session;
        if($session['header_styling'] != null) return $session['header_styling'];
        else return 1;
    } 

    public function actionSdStyling()
    {
        $session = Yii::$app->session;
        if($session['sd_styling'] != null) return $session['sd_styling'];
        else return 1;
    } 

    public function actionCnGradiyent()
    {
        $session = Yii::$app->session;
        if($session['cn_gradiyent'] != null) return $session['cn_gradiyent'];
        else return 1;
    } 

    public function actionCnStyle()
    {
        $session = Yii::$app->session;
        if($session['cn_style'] != null) return $session['cn_style'];
        else return 1;
    } 
    public function actionBoxed()
    {
        $session = Yii::$app->session;
        if($session['boxed'] != null) return $session['boxed'];
        else return 1;
    } 

}

