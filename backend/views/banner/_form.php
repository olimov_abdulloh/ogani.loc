<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <div id="image" class="col-md-12">
                <?=Html::img($model->getImage(), [
                    'style' => 'width:200px; height:200px;object-fit: cover;',
                ])?>
            </div>
            <br>
            <div class="col-md-12">
                <?= $form->field($model, 'file')->fileInput(['class'=>"image_input",'id'=>'inputFile']); ?>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'text')->textarea(['rows' => 3]) ?>
                </div>
                <div class="col-md-9">
                    <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

                </div>
                <div class="col-md-3">
                    <label for="">Статус</label>
                    <?= $form->field($model, 'status')->widget(\dosamigos\switchery\Switchery::className(), [
                        'options' => [
                            'label' => false
                        ],
                        'clientOptions' => [
                            'color' => '#5fbeaa',
                        ]
                    ])->label(false);?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'url_banner')->textInput(['maxlength' => true]) ?>

                </div>
            </div>
        </div>
    </div>


  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
