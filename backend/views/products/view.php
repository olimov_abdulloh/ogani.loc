<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
?>
<div class="products-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'subcategory_id',
            'text:ntext',
            'price',
            'old_price',
            'currency_id',
            'region_id',
            'district_id',
            'status',
            'top:boolean',
        ],
    ]) ?>

</div>
