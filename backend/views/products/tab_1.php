<?php
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
use dosamigos\switchery\Switchery;

?>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'category_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getCategoryList(),
                'options' => [
                    'placeholder' => 'Выберите',
                    'onchange'=>'
                        $.post( "/admin/products/subcategory?id='.'"+$(this).val(), function( data ){
                            $( "select#sub_categorys_id" ).html( data);
                        });'
                ],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'subcategory_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getSubategoryid($model->category_id),
                'options' => [
                    'placeholder' => 'Выберите',
                    'id' => 'sub_categorys_id',],
                'size' => kartik\select2\Select2::SMALL,
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'old_price')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'currency_id')->dropDownList($model->getCurrencyList(), []) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList($model->getStatus(), ['prompt' => 'Выберите статус']) ?>
        </div>
        <div class="col-md-3">
            <label for="">Премиум</label>
            <?= $form->field($model, 'top')->widget(Switchery::className(), [
                'options' => [
                    'label' => false
                ],
                'clientOptions' => [
                    'color' => '#5fbeaa',
                ]
            ])->label(false);?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
        </div>
    </div>










