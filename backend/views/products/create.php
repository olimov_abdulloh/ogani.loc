<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Products */

?>
<div class="products-create">
    <?= $this->render('_form', [
        'model' => $model,
        'upload_images' => $upload_images,
        'post' => $post,
    ]) ?>
</div>
