<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $upload_images  */
/* @var $post  */
/* @var $form yii\widgets\ActiveForm */

$this->title = "Продукты";
$this->params['breadcrumbs'][] = ['label' => "Продукты", 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Создать' : 'Изменить';
?>


<?php $form = ActiveForm::begin(); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#default-tab-1" data-toggle="tab">Описание</a></li>
        <li class=""><a href="#default-tab-2" data-toggle="tab">Фото (<?= count($upload_images)?>)</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="default-tab-1" style="margin-left:30px;">
            <?= $this->render('tab_1',[
                    'form' => $form,
                    'model' => $model,
            ])?>
        </div>
        <div class="tab-pane fade" id="default-tab-2">
            <?= $this->render('tab_2',[
                'form' => $form,
                'model' => $model,
                'upload_images' => $upload_images,
                'post' => $post,
            ])?>
        </div>

    </div>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Сохранить' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <a href="/admin/products/index" class="btn btn-inverse">Назад</a>
</div>
<?php ActiveForm::end(); ?>




